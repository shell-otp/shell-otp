
Would you use Google Authenticator just with mobile devices?
(UNIX USERS ONLY)

INSTALL:<br/>

* brew install oath-toolkit (if Mac OS X, with Homebrew)<br/>
* apt-get install oathtool (if Linux, dpkg-based)<br/>
* yum install oathtool (if Linux, rpm-based)<br/>

* if you are OSX user and want automatic copy to clipboard, install PyObjC (http://pythonhosted.org/pyobjc/install.html) <br/>

- Download the project 
- mv shell-otp-data into your $HOME
- edit $HOME/shell-otp-data/mysecret with your secret b32 word 
- generate token with: 
         $  python googleauth.py [-f secret filename ] [-d data directory]

