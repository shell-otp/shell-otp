import argparse
import subprocess
from platform import uname
from os import environ, system, devnull
from shlex import split
from AppKit import NSPasteboard, NSStringPboardType, NSString, NSUTF8StringEncoding 

def pbcopy(s): 
    pb = NSPasteboard.generalPasteboard() 
    pb.declareTypes_owner_([NSStringPboardType], None) 
    newStr = NSString.stringWithString_(s) 
    newData = newStr.nsstring().dataUsingEncoding_(NSUTF8StringEncoding) 
    pb.setData_forType_(newData, NSStringPboardType)

def main():

    OATH_KEY_HOME = environ['HOME'] + "/shell-otp-data/"
    OATH_SECRET = 'mysecret'
    OATH_DB = OATH_KEY_HOME + 'oath.db'
    OATH_COMMAND = "/usr/local/bin/oathtool --hotp -b -d 6 -c "
    OSA_COMMAND = """osascript -e 'tell app "System Events" to display alert '"""
    #PBCOPY_COMMAND = '|pbcopy'

    parser = argparse.ArgumentParser(prog='googleauth.py: life is too short to make token')
    parser.add_argument('-f', '--file', help='What is your b32 secret?',)
    parser.add_argument('-d', '--dir', help='where is secret file?')
    args = vars(parser.parse_args())


    if args['dir']:OATH_KEY_HOME = args['dir']
    if args['file']: OATH_SECRET = args['file']

    #print "Token for " + OATH_SECRET + " in progress...",

    counter = 0

    with open(OATH_DB,"r+") as db:
        counter = int(db.read())
        counter +=1
        db.truncate()
        OATH_COMMAND += (str(counter))
    if not db.closed:
        db.close()
    #print 'DB file is closed: ' + str(db.closed)

    with open(OATH_DB,"w") as db:
        db.write(str(counter))
    if not db.closed:
        db.close()
    #print 'DB file is closed: ' + str(db.closed)


    with open(OATH_KEY_HOME+OATH_SECRET,"r") as keyfile:
        secret = keyfile.read().rstrip()
        OATH_COMMAND += ' ' + secret
    if not keyfile.closed:
        keyfile.close()
    #print 'Key file is closed: ' + str(db.closed)


    p_main = subprocess.Popen(OATH_COMMAND, shell=True, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
    output = ''
    error_pb = ''

    for line in iter(p_main.stdout.readline, b''):
        output += line

    # OSX Alert 
    my_platform = uname()
    if 'Darwin' in my_platform:
        
        #PBCOPY_COMMAND = 'echo ' + output.rstrip() + PBCOPY_COMMAND
        #os.system(PBCOPY_COMMAND) 
	pbcopy(str(output))

        OSA_COMMAND =  OSA_COMMAND + output.rstrip()
	with open(devnull, "w") as fnull:
	    result = subprocess.call(OSA_COMMAND, shell=True, stdout=fnull, stderr=fnull)

        #for line in iter(p_osa.stderr.readline, b''):
        #    error_pb += line
        #if len(error_pb) > 0: print error_pb

    print output




if __name__ == '__main__':
    main()
